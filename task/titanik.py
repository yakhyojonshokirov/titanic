import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    # Filter the data for the specified titles
    titles = ["Mr.", "Mrs.", "Miss."]
    filtered_data = data[data['Name'].str.extract(r'\b(\w+\.)', expand=False).isin(titles)]

    # Calculate the number of missing values and median values for each group
    results = []
    for title in titles:
        missing_values = filtered_data[filtered_data['Name'].str.extract(r'\b(\w+\.)', expand=False) == title]['Age'].isna().sum()
        median_value = filtered_data[filtered_data['Name'].str.extract(r'\b(\w+\.)', expand=False) == title]['Age'].median()
        results.append((title, missing_values, round(median_value)))

    return results
